package com.example.focus_start_task.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.focus_start_task.domain.model.Currency
import com.example.focus_start_task.domain.usecase.ConvertCurrencyUseCase
import com.example.focus_start_task.domain.usecase.GetDataFromAPIUseCase
import com.example.focus_start_task.domain.usecase.LoadDataUseCase
import com.example.focus_start_task.domain.usecase.SaveDataUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException

class MainViewModel (
	private val getDataFromAPIUseCase: GetDataFromAPIUseCase,
	private val saveDataUseCase: SaveDataUseCase,
	private val loadDataUseCase: LoadDataUseCase,
	private val convertCurrencyUseCase: ConvertCurrencyUseCase,
) : ViewModel() {

	var currencyList : MutableLiveData<ArrayList<Currency>> = MutableLiveData()
	var convertCurrency : MutableLiveData<Double> = MutableLiveData()
	var errorDataDownloadToast : MutableLiveData<Boolean> = MutableLiveData()
	var errorDataSaveToast : MutableLiveData<Boolean> = MutableLiveData()
	var noInternetConnectionToast : MutableLiveData<Boolean> = MutableLiveData()

	private var DELAY = 30000L //5min

	init {
		val loadedData : ArrayList<Currency>? = loadDataUseCase.execute()

		if(loadedData.isNullOrEmpty()) getDataFromAPI()
		else currencyList.value = loadedData!!

		viewModelScope.launch {
			while(true) {
				try {
					noInternetConnectionToast.value = false
					delay(DELAY)
					val result = withContext(Dispatchers.IO) { getDataFromAPIUseCase.execute() }
					if (result == null) {
						errorDataDownloadToast.value = true
					} else {
						errorDataDownloadToast.value = false
						currencyList.value = result!!
					}
				} catch (e: IOException) {
					noInternetConnectionToast.value = true
				}
			}
		}
	}

	fun getDataFromAPI() {
		viewModelScope.launch {
			try {
				val result = withContext(Dispatchers.IO) { getDataFromAPIUseCase.execute() }

				if(result == null) {
					errorDataDownloadToast.value = true
				} else {
					errorDataDownloadToast.value = false
					currencyList.value = result!!
				}

				noInternetConnectionToast.value = false
			} catch (e: IOException) {
				noInternetConnectionToast.value = true
			}
		}
	}

	fun convertCurrency(currency : Currency, value : Double) {
		convertCurrency.value = convertCurrencyUseCase.execute(currency, value)
	}

	override fun onCleared() {
		val saveData = currencyList.value
		if(saveData == null) {
			errorDataSaveToast.value = true
		} else {
			errorDataDownloadToast.value = false
			saveDataUseCase.execute(saveData)
		}
		super.onCleared()
	}
}