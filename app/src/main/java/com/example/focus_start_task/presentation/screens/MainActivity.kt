package com.example.focus_start_task.presentation.screens

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.focus_start_task.R
import com.example.focus_start_task.domain.model.Currency
import com.example.focus_start_task.presentation.CurrencyListRecyclerViewAdapter
import com.example.focus_start_task.presentation.MainViewModel
import com.example.focus_start_task.presentation.MainViewModelFactory

class MainActivity : AppCompatActivity() {
	private lateinit var vm : MainViewModel
	private val context = this

	override fun onCreate(savedInstanceState: Bundle?) {

		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		val recyclerView : RecyclerView = findViewById(R.id.rv_currency)
		recyclerView.layoutManager = LinearLayoutManager(context)

		val currencyListRecyclerViewAdapter = CurrencyListRecyclerViewAdapter( ArrayList(), object: CurrencyListRecyclerViewAdapter.OnCurrencyListClickListener {
			override fun onStateClick(currency: Currency, position: Int) {

				val dialogView = LayoutInflater.from(context).inflate(R.layout.convert_dialog, null)
				val mDialogBuilder = AlertDialog.Builder(context).setView(dialogView)

				val inputET : EditText= dialogView.findViewById(R.id.et_value_in)
				val valueRubTV : TextView = dialogView.findViewById(R.id.tv_value_rub)
				valueRubTV.visibility = View.INVISIBLE


				val convertBtn : Button = dialogView.findViewById(R.id.btn_dialog_convert)
				convertBtn.setOnClickListener {
					val input = inputET.text
					if(input.isNullOrEmpty()) {
						showToast("Enter value before conversion")
					} else {
						vm.convertCurrency(currency, input.toString().toDouble())
						vm.convertCurrency.observe(context, Observer { valueRubTV.text = String.format("%.2f", it) })
						valueRubTV.visibility = View.VISIBLE
					}
				}

				mDialogBuilder
					.setCancelable(false)
					.setNegativeButton("Exit") { dialog, id ->
						dialog?.cancel()
						valueRubTV.visibility = View.INVISIBLE
					}
				mDialogBuilder.create().show()
			}
		})
		recyclerView.adapter = currencyListRecyclerViewAdapter


		vm = ViewModelProvider(this, MainViewModelFactory(context)).
			get(MainViewModel::class.java)


		val refreshButton: Button =  findViewById(R.id.btn_refresh)
		refreshButton.setOnClickListener { vm.getDataFromAPI() }

		vm.currencyList.observe(context, Observer {
			currencyListRecyclerViewAdapter.updateData(it)
		})
		vm.errorDataDownloadToast.observe(context, Observer {
			if(it) showToast("Failed to update data")
		})
		vm.errorDataSaveToast.observe(context, Observer {
			if(it) showToast("Failed to save data")
		})
		vm.noInternetConnectionToast.observe(context, Observer {
			if(it) showToast("No internet Connection")
		})
	}

	private fun showToast(message: String) {
		Toast.makeText(
			context,
			message,
			Toast.LENGTH_SHORT
		).show();
	}

}