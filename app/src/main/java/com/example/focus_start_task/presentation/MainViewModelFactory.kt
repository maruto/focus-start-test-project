package com.example.focus_start_task.presentation

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.focus_start_task.data.repository.RepositoryImpl
import com.example.focus_start_task.domain.model.Currency
import com.example.focus_start_task.domain.usecase.ConvertCurrencyUseCase
import com.example.focus_start_task.domain.usecase.GetDataFromAPIUseCase
import com.example.focus_start_task.domain.usecase.LoadDataUseCase
import com.example.focus_start_task.domain.usecase.SaveDataUseCase

class MainViewModelFactory (
	context: Context,
) : ViewModelProvider.Factory {

	private val repository by lazy(LazyThreadSafetyMode.NONE) {
		RepositoryImpl(context = context)
	}

	private val getDataFromAPIUseCase by lazy(LazyThreadSafetyMode.NONE) {
		GetDataFromAPIUseCase(repository = repository)
	}

	private val saveDataUseCase by lazy(LazyThreadSafetyMode.NONE) {
		SaveDataUseCase(repository = repository)
	}

	private val convertCurrencyUseCase by lazy(LazyThreadSafetyMode.NONE) {
		ConvertCurrencyUseCase()
	}

	private val loadDataUseCase by lazy(LazyThreadSafetyMode.NONE) {
		LoadDataUseCase(repository = repository)
	}

	override fun <T : ViewModel> create(modelClass: Class<T>): T {
		return MainViewModel(
			getDataFromAPIUseCase = getDataFromAPIUseCase,
			saveDataUseCase = saveDataUseCase,
			loadDataUseCase = loadDataUseCase,
			convertCurrencyUseCase = convertCurrencyUseCase,
		) as T
	}
}