package com.example.focus_start_task.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.focus_start_task.R
import com.example.focus_start_task.domain.model.Currency

class CurrencyListRecyclerViewAdapter(
	var currencyList: ArrayList<Currency>,
	private val onClickListener: OnCurrencyListClickListener,
) : RecyclerView.Adapter<CurrencyListRecyclerViewAdapter.CurrencyViewHolder>() {

	interface OnCurrencyListClickListener {
		fun onStateClick(currency : Currency, position: Int);
	}


	class CurrencyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

		val nameOfCurrencyTV: TextView = itemView.findViewById(R.id.tv_currency_name)
		val valueOfCurrencyTV: TextView = itemView.findViewById(R.id.tv_value)
		val differenceOfCurrencyTV: TextView = itemView.findViewById(R.id.tv_difference)
	}

	fun updateData(newCurrencyList: ArrayList<Currency>) {
		currencyList = newCurrencyList
		notifyDataSetChanged()
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
		val itemView =
			LayoutInflater.from(parent.context)
				.inflate(R.layout.list_item, parent, false)

		return CurrencyViewHolder(itemView)
	}

	override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {

		if(currencyList[position].nominal == 1) {
			holder.nameOfCurrencyTV.text = currencyList[position].name
		} else {
			holder.nameOfCurrencyTV.text = currencyList[position].nominal.toString().toString() + " " +  currencyList[position].name
		}
		holder.valueOfCurrencyTV.text = currencyList[position].value.toString()
		val difference = currencyList[position].difference
		if(difference >= 0) {
			holder.differenceOfCurrencyTV.text = String.format("+%.4f", +difference)
			holder.differenceOfCurrencyTV.setTextColor(
				ContextCompat.getColor(holder.differenceOfCurrencyTV.context, R.color.negativeDifference))
		} else {
			holder.differenceOfCurrencyTV.text = String.format("%.4f", +difference)
			holder.differenceOfCurrencyTV.setTextColor(
				ContextCompat.getColor(holder.differenceOfCurrencyTV.context, R.color.positiveDifference))
		}


		holder.itemView.setOnClickListener {
			onClickListener.onStateClick(currencyList[position], position)
		}
	}

	override fun getItemCount() = currencyList.size

	fun getData() : ArrayList<Currency> {
		return currencyList
	}
}