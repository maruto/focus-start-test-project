package com.example.focus_start_task.domain.usecase

import  com.example.focus_start_task.domain.model.Currency

class ConvertCurrencyUseCase {

	fun execute(currency : Currency, value : Double) : Double {
		return (currency.value/currency.nominal)*value
	}
}