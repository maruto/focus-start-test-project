package com.example.focus_start_task.domain.usecase

import com.example.focus_start_task.domain.model.Currency
import com.example.focus_start_task.domain.repository.Repository

class GetDataFromAPIUseCase(private val repository: Repository) {

	fun execute() : ArrayList<Currency>? {
		return repository.getDataFromAPI()
	}
}