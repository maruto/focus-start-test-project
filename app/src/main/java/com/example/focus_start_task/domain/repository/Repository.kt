package com.example.focus_start_task.domain.repository

import com.example.focus_start_task.domain.model.Currency

interface Repository {

	fun getDataFromAPI() : ArrayList<Currency>?

	fun saveData(currencyList: ArrayList<Currency>)

	fun loadData() : ArrayList<Currency>?
}