package com.example.focus_start_task.domain.usecase

import com.example.focus_start_task.domain.model.Currency
import com.example.focus_start_task.domain.repository.Repository

class SaveDataUseCase(private val repository: Repository) {

	fun execute(currencyList: ArrayList<Currency>) {
		repository.saveData(currencyList)
	}
}