package com.example.focus_start_task.domain.model

data class Currency(
	val name: String,
	val value: Double,
	val previousValue: Double,
	val nominal: Int, ) {

	val difference = previousValue - value
}