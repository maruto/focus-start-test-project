package com.example.focus_start_task.domain.requestAPI

import retrofit2.Call
import retrofit2.http.GET

interface RequestAPI {

	@GET("daily_json.js")
	fun getStringJsonResponse(): Call<String>
}