package com.example.focus_start_task.domain.requestAPI

import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

object RetrofitInstance {
	private val retrofit by lazy {
		Retrofit.Builder()
			.baseUrl("https://www.cbr-xml-daily.ru/")
			.addConverterFactory(ScalarsConverterFactory.create())
			.build()
	}

	val api: RequestAPI by lazy {
		retrofit.create(RequestAPI::class.java)
	}
}
