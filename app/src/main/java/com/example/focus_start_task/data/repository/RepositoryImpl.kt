package com.example.focus_start_task.data.repository

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.example.focus_start_task.domain.model.Currency
import com.example.focus_start_task.domain.repository.Repository
import com.example.focus_start_task.domain.requestAPI.RetrofitInstance
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

class RepositoryImpl (private val context: Context) : Repository {

	private val FILE_NAME = "StorageCurrencyList"
	private val SAVED_TEXT = "CurrencyList"
	private val NAME = "Name"
	private val VALUE = "Value"
	private val PREVIOUS = "Previous"
	private val NOMINAL = "Nominal"

	override fun getDataFromAPI() : ArrayList<Currency>? {
		if(!isNetworkAvailable()) {
			throw IOException("No internet connection.")
		}

		val response = RetrofitInstance.api.getStringJsonResponse().execute()

		val currencyList : ArrayList<Currency>

		if(response.isSuccessful) {
			val stringResponse = response.body()
			if(stringResponse == null) {
				return null
			} else {
				val responseCurrency = JSONObject(stringResponse)
				val valutes: JSONObject = responseCurrency.getJSONObject("Valute")

				val valutesNames = valutes.names()
				if(valutesNames==null) {
					return null
				} else {
					currencyList = ArrayList()
					for (i in 0 until valutesNames.length()) {
						val currentCurrency = valutes.getJSONObject(valutesNames.getString(i))
						val newCurrency = Currency(
							currentCurrency.getString(NAME),
							currentCurrency.getDouble(VALUE),
							currentCurrency.getDouble(PREVIOUS),
							currentCurrency.getInt(NOMINAL),
						)
						currencyList.add(newCurrency)
					}
				}
			}
		} else {
			return null
		}
		return currencyList
	}

	override fun saveData(currencyList: ArrayList<Currency>) {
		val jsonList = JSONArray()
		for(currency in currencyList) {
			val currencyJSON = JSONObject()
			currencyJSON.put(NAME, currency.name)
			currencyJSON.put(VALUE, currency.value)
			currencyJSON.put(PREVIOUS, currency.previousValue)
			currencyJSON.put(NOMINAL, currency.nominal)
			jsonList.put(currencyJSON)
		}

		val sPref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
		val ed: SharedPreferences.Editor = sPref.edit()
		ed.putString(SAVED_TEXT, jsonList.toString())
		ed.apply()
	}

	override fun loadData() : ArrayList<Currency>? {
		val sPref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
		val stringJSON: String? = sPref.getString(SAVED_TEXT, null)

		if (stringJSON != null) {
			val arrayJSON = JSONArray(stringJSON)
			val newCurrencyList = ArrayList<Currency>()
			for (i in 0 until arrayJSON.length()) {
				val currentCurrency = arrayJSON.getJSONObject(i)
				val newCurrency = Currency(
					currentCurrency.getString(NAME),
					currentCurrency.getDouble(VALUE),
					currentCurrency.getDouble(PREVIOUS),
					currentCurrency.getInt(NOMINAL),
				)
				newCurrencyList.add(newCurrency)
			}
			return newCurrencyList
		} else {
			return null
		}
	}

	private fun isNetworkAvailable(): Boolean {
		val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			val nw      = connectivityManager.activeNetwork ?: return false
			val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
			return when {
				actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
				actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
				//for other device how are able to connect with Ethernet
				actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
				//for check internet over Bluetooth
				actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
				else -> false
			}
		} else {
			val nwInfo = connectivityManager.activeNetworkInfo ?: return false
			return nwInfo.isConnected
		}
	}
}